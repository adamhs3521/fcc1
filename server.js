var express=require('express')
var app=express();
var time=require('./dates')

var port=process.env.PORT||3000



app.listen(port,function(){
    console.log("Express server is listening on port "+port)
})

app.use('/',express.static('./public'))


app.get('/:date', function(req,res){
    res.json(time.timeStampConverter(req.params.date))
})