function isDate(d){
    var date = new Date(d);
    
    return(date instanceof Date && !isNaN(date.valueOf()));
}

function unixConverter(timeStamp){
    var months=["January","February","March","April",
                "May","June","July","August",
                "September","October","November","December"]
    var date=new Date(timeStamp*1000)//js uses milliseconds, unix is in seconds
  
    return(months[date.getMonth()]+" "+date.getDate()+" , "+date.getFullYear())
}



function makePretty(date){
    var month=date.split(/[0-9]/)[0];
    var dateArray=new Date(date).toString().split(' ')
    var year=dateArray[3]
    var day=dateArray[2]
    
    month=month.charAt(0).toUpperCase()
                +month.substr(1)
    
    return month+' '+day+', '+year
}


var timeStampConverter =function(query){
    var natural=null,unix=null;

    if (!isNaN(query)){ 
        natural= unixConverter(query); 
        unix=query}
    else if(isDate(query)){
        natural=makePretty(query); 
        unix=Date.parse(query)/1000
    }
    return ({natural:natural,unix:unix})
}

module.exports={
  timeStampConverter:timeStampConverter
}


